import React from 'react';
import './App.css';
import UsersListContainer from './components/UserListContainer.jsx';

function App() {
  return (
    <div className="App">
      <UsersListContainer />
    </div>
  );
}

export default App;
