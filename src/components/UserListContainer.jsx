import React, { Component } from 'react';
import axios from 'axios';
import UsersList from './UsersList.jsx';

class UsersListContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            users: []
        }
    }

    handleUsersSorting = (sortType) => {
        const { users } = this.state;
        switch (sortType) {
            case 'asc':
                const sortedAscending = users.sort((a, b) => a.last_name > b.last_name);
                this.setState({
                    users: sortedAscending
                });
                break;
            case 'desc':
                const sortedDescending = users.sort((a, b) => b.last_name > a.last_name);
                this.setState({
                    users: sortedDescending
                });
                break;
            default:
                break;
        }
    }

    async componentDidMount() {
        const response = await axios.get('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json');
        console.log(response);
        const initialData = response.data.sort((a, b) => a.last_name > b.last_name);
        this.setState({
            isLoading: false,
            users: initialData
        });
    }

    render() {
        return (
            <UsersList 
                users={this.state.users}
                usersSorting={this.handleUsersSorting}
                isLoading={this.state.isLoading}
            />
        );
    }
}

export default UsersListContainer;