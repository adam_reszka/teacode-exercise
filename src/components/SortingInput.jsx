import React from 'react';

const SortingInput = props => {

    const handleChange = e => {
        props.onUsersSort(e.target.value);
    }

    return (
        <select
            onChange={handleChange}
            defaultValue="asc"
        >
            <option value="asc">Ascendent</option>
            <option value="desc">Descendent</option>
        </select>
    )
}

export default SortingInput;