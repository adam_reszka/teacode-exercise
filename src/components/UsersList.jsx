import React, { useState } from 'react';
import styled from '@emotion/styled';
import SearchBar from './SearchBar.jsx';
import UsersListHeader from './UsersListHeader';
import UsersListItem from './UsersListItem.jsx';
import UsersListControls from './UsersListControls.jsx';

const ListBox = styled('div')`
    margin-top: 6rem;
    width: 56rem;
    background-color: #fff;
    border-radius: .6rem;
    overflow: hidden;
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.4);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.4);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.4);
    box-sizing: borded-box;
`;

const List = styled('ul')`
    padding: .8rem 2rem;
    list-style-type: none;
`;

const Loading = styled('p')`
    padding: .8rem 2.5rem;
    font-size: 1.4rem;
`;

const UsersList = props => {

    const listTitle = 'Users List';
    const { users, isLoading } = props;
    const [searchString, setSearchString] = useState(null);

    const userSearching = searchPhrase => {
        setSearchString(searchPhrase);
    }
    const usersSorting = sortType => {
        props.usersSorting(sortType);
    }

    return (
        <ListBox>
            <UsersListHeader
                title={listTitle}
            />
            <SearchBar
                onUserSearch={userSearching}
            />
            <UsersListControls
                onUsersSort={usersSorting}
            />
            {
                isLoading ?
                    <Loading>Loading data...</Loading> :
                    <List>
                        {users.filter((data) => {
                            let _currentData;
                            if (searchString == null) {
                                _currentData = data;
                            } else if (data.first_name.toLowerCase().includes(searchString.toLowerCase()) ||
                                data.last_name.toLowerCase().includes(searchString.toLowerCase())) {
                                _currentData = data;
                            }
                            return _currentData;
                        }).map(user => 
                            user && (
                                <UsersListItem 
                                    singleUser={user}
                                    key={user.id}
                                />
                            )
                        )}
                    </List>
            }
            
        </ListBox>
    );
}

export default UsersList;