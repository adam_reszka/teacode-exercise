import React from 'react';
import styled from '@emotion/styled';
import SortingInput from './SortingInput';


const Controls = styled('div')`
    display: flex;
    padding: .8rem 2rem;
    align-items: center;
`;

const Label = styled('label')`
    font-size: 1.4rem;
    padding: 0 1rem 0 .6rem;
`;

const UsersListControls = props => {

    const usersSorting = sortType => {
        props.onUsersSort(sortType);
    }

    return (
        <Controls>
            <Label>Sort:</Label>
            <SortingInput
                onUsersSort={usersSorting}
            />
        </Controls>
    );
}

export default UsersListControls;