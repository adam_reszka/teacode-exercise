import React, { useState } from 'react';
import styled from '@emotion/styled';
import avatar_placeholder from './../assets/avatar_placeholder.png';

const Item = styled('li')`
    padding: .6rem;
    cursor: pointer;
    border-radius: .6rem;
    margin-bottom: .8rem;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-size: 1.4rem;
    position: relative;
    border: 1px solid #ecf0f1;

    &:hover {
        background-color: #ecf0f1;
    }

    &.selected {
        background-color: #3498db;
        border-color;
        color: #fff;
    }
`;

const Avatar = styled('div')`
    border-radius: 50%;
    width: 4rem;
    height: 4rem;
    margin: .5rem;
    overflow: hidden;
    background-color: #fff;
`;

const AvatarImage = styled('img')`
    display: block;
    max-width: 100%;
`;

const Checkbox = styled('input')`
    position: absolute;
    right: .8rem;
    top: 50%;
    transform: translate(0, -53%);
`;

const UsersListItem = props => {

    const { singleUser } = props;
    const { id, avatar, first_name, last_name } = singleUser;
    const full_name = `${first_name} ${last_name}`;
    const [isSelected, setIsSelected] = useState(false);
    const avatarSource = avatar ? avatar : avatar_placeholder;

    const handleClick = () => {
        console.log("Current User: ", id);
        setIsSelected(!isSelected);
    }

    return (
        <Item
            onClick={handleClick}
            className={isSelected ? 'selected' : undefined}
        >
            <Avatar>
                <AvatarImage src={avatarSource} alt="user logo"/>
            </Avatar>
            
            {full_name}
            <Checkbox
                type="checkbox"
                checked={isSelected}
                readOnly
            />
        </Item>
    )
}

export default UsersListItem;