import React from 'react';
import styled from '@emotion/styled';

const InputContainer = styled('div')`
    box-sizing: border-box;
    padding: 2rem 2rem .8rem;
    width: 100%;
`;
const Input = styled('input')`
    box-sizing: border-box;
    width: 100%;
    padding: .6rem;
`;

const SearchBar = props => {

    const handleChange = e => {
        props.onUserSearch(e.target.value);
    }

    return (
        <InputContainer>
            <Input
                type="text"
                placeholder="Search user"
                onChange={handleChange}
            />
        </InputContainer>
    );
}

export default SearchBar;