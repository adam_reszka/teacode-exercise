import React from 'react';
import styled from '@emotion/styled';

const Header = styled('div')`
    background-color: #3498db;
`;

const Title = styled('p')`
    color: #fff;
    font-size: 2rem;
    margin: 0;
    padding: 2rem;
`;

const UsersListHeader = props => {
    const { title } = props;

    return (
        <Header>
            <Title>
                {title}
            </Title>
        </Header>
    )
}

export default UsersListHeader;